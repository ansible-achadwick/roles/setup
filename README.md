Role "Setup"
=========

Used to bootstrap newly provisioned OS

Molecule test: [![pipeline status](https://gitlab.com/ansible-achadwick/roles/setup/badges/master/pipeline.svg)](https://gitlab.com/ansible-achadwick/roles/setup/-/commits/master) 

Example Playbook
----------------

```
- name: Initial bootstrapping play with root user
  hosts: all
  remote_user: root
  vars:
    - ansible_ssh_private_key_file: "~/.ssh/remote-root"
  
  roles:
    - setup
```

Include this role using an Ansible Galaxy requirements.yml file. E.g.:
```
# from GitLab or other git-based scm
- src: git+https://gitlab.com/ansible-achadwick/roles/setup.git
  scm: git
  version: "0.1.6"  # quoted, so YAML doesn't parse this as a floating-point value
  name: setup
```
And running:
```
ansible-galaxy role install -r requirements.yml
```

License
-------

BSD
